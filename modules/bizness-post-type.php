<?php

/**
 * Creates Mega Menu Post Type
 * ................................................................
 */

// Initialize
//................................................................
StaticBlockContent::onload();

// Easy access to static block output
//................................................................
function the_static_block( $id = false, $args = array() ) {
	if ($id) {
		$args["id"] = $id;
		echo StaticBlockContent::get_static_content($args);
	}
}

#-----------------------------------------------------------------
# Static Block Class
#-----------------------------------------------------------------
class StaticBlockContent {
	static function onload() {
		add_action('init', array(__CLASS__,'init_static_blocks'));
		add_action("after_switch_theme", "flush_rewrite_rules", 10 ,  2); // update permalinks for new rewrite rules
		add_shortcode('static_content', array(__CLASS__,'static_content_shortcode'));
		add_shortcode('bizness_mega_menu', array(__CLASS__,'static_content_shortcode'));
	}
	static function init_static_blocks() {
		if (function_exists('register_post_type')) {
			register_post_type('bizness_mega_menu',
				array(
					'labels' => array(
							'name' =>				esc_html_x('Mega Menu Post', 'post type general name', 'bizness'),
							'singular_name' =>		esc_html_x('Mega Menu', 'post type singular name', 'bizness'),
							'add_new' =>			esc_html_x('Add New', 'block', 'bizness'),
							'add_new_item' =>		esc_html__('Add New Mega Menu', 'bizness'),
							'edit_item' =>			esc_html__('Edit Mega Menu', 'bizness'),
							'new_item' =>			esc_html__('New Mega Menu', 'bizness'),
							'all_items' =>			esc_html__('Mega Menus', 'bizness'),
							'view_item' =>			esc_html__('View Mega Menu', 'bizness'),
							'search_items' =>		esc_html__('Search', 'bizness'),
							'not_found' =>			esc_html__('No Mega Menus found', 'bizness'),
							'not_found_in_trash' =>	esc_html__('No Mega Menus found in Trash', 'bizness'), 
							'parent_item_colon' => '',
							'menu_name' => 'Mega Menu'
						),
					'exclude_from_search' => true,
					'publicly_queryable'  => true,
					'public'              => true,
					'show_ui'             => true,
					'query_var'           => 'bizness_mega_menu',
					'rewrite'             => array('slug' => 'bizness_mega_menu'),
					'supports'            => array(
						'title',
						'editor',
						'thumbnails',
						'revisions',
					),
				)
			);
		}
	}
}

#-----------------------------------------------------------------
# Services
#-----------------------------------------------------------------
function bizness_service() {		

	# Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Service', 'Post Type General Name' ),
		'singular_name'       => _x( 'Service', 'Post Type Singular Name' ),
		'menu_name'           => __( 'Service' ),
		'parent_item_colon'   => __( 'Parent Service' ),
		'all_items'           => __( 'All Services' ),
		'view_item'           => __( 'View Service' ),
		'add_new_item'        => __( 'Add New Service' ),
		'add_new'             => __( 'Add Service ' ),
		'edit_item'           => __( 'Edit Service' ),
		'update_item'         => __( 'Update Service' ),
		'search_items'        => __( 'Search Service' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' )
	);
		
	# Set other options for Custom Post Type...
	$args = array(
		'labels'              => $labels,
		'label'               => __( 'service' ),
		'description'         => __( 'Service news and reviews' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'canesc_html_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'supports'            => array( 'title','thumbnail','editor' ),
		'menu_icon'		      => 'dashicons-media-spreadsheet'	
	);
	register_post_type( 'bizness-service', $args );
	
}
add_action( 'init', 'bizness_service', 0 );

function bizness_service_categories_taxonomies()
{
	 register_taxonomy( 'service_cat', 'bizness-service', array(
	  'hierarchical' => true,
	  'label' => 'Categories',
	  'query_var' => true,
	  'rewrite' => true
	 ));
}
 
add_action( 'init', 'bizness_service_categories_taxonomies', 0);

function register_bizness_service_meta() {
    add_meta_box( 
        'bizness_service_meta_id',                           				# Metabox
        __( 'Details', 'bizness' ),           								# Title
        'service_meta',                               					# Call Back func
       	'bizness-service',                                         		# post type
        'normal'                                            			# Text Content
    );                                                  				# Text Content  
}
add_action( 'add_meta_boxes', 'register_bizness_service_meta' );

#-----------------------------------------------------------------
# Service Meta
#-----------------------------------------------------------------
function service_meta( $post, $args ){

	wp_nonce_field( basename( __FILE__ ), 'bizness_meta_box_nonce' );
	$teacher_label					= get_post_meta($post->ID, 'teacher_label', true);
	$teacher_name					= get_post_meta($post->ID, 'teacher_name', true);	
	$teacher_image_url				= get_post_meta($post->ID, 'teacher_image_url', true);	
	$teacher_designation			= get_post_meta($post->ID, 'teacher_designation', true);
	$teacher_details				= get_post_meta($post->ID, 'teacher_details', true);	
	$teacher_fb_url					= get_post_meta($post->ID, 'teacher_fb_url', true);
	$teacher_twitter_url		    = get_post_meta($post->ID, 'teacher_twitter_url', true);
	$teacher_dribbble_url		    = get_post_meta($post->ID, 'teacher_dribbble_url', true);

?>
<div class="wrap">
	<table class="form-table">

		<tr valign="top">
			<th scope="row">
				<label for="teacher_label"><?php esc_html_e('Teacher/Speaker Label', 'bizness')?></label>
			</th>
			<td style="vertical-align: middle;">
				<input type="text" name="teacher_label" class="widefat" value="<?php if(!empty($teacher_label)){echo esc_attr($teacher_label);}?>">
			</td>
		</tr>
		<!-- End Teacher/Speaker Label -->	

		<tr valign="top">
			<th scope="row">
				<label for="teacher_name"><?php esc_html_e('Teacher/Speaker Name', 'bizness')?></label>
			</th>
			<td style="vertical-align: middle;">
				<input type="text" name="teacher_name" class="widefat" value="<?php if(!empty($teacher_name)){echo esc_attr($teacher_name);}?>">
			</td>
		</tr>
		<!-- End Teacher/Speaker Name -->

		<tr valign="top">
			<th scope="row">
				<label for="teacher_image_url"><?php esc_html_e('Teacher/Speaker Image URL', 'bizness')?></label>
			</th>
			<td style="vertical-align: middle;">
				<input type="text" name="teacher_image_url" class="widefat" value="<?php if(!empty($teacher_image_url)){echo esc_url($teacher_image_url);}?>">
			</td>
		</tr>
		<!-- End Teacher/Speaker Designation -->


		<tr valign="top">
			<th scope="row">
				<label for="teacher_designation"><?php esc_html_e('Teacher/Speaker Designation', 'bizness')?></label>
			</th>
			<td style="vertical-align: middle;">
				<input type="text" name="teacher_designation" class="widefat" value="<?php if(!empty($teacher_designation)){echo esc_attr($teacher_designation);}?>">
			</td>
		</tr>
		<!-- End Teacher/Speaker Designation -->			

		<tr valign="top">
			<th scope="row">
				<label for="teacher_details"><?php esc_html_e('Teacher/Speaker Details', 'bizness')?></label>
			</th>
			<td style="vertical-align: middle;">
				<textarea name="teacher_details" rows="5" cols="5" class="widefat"><?php if(!empty($teacher_details)){echo esc_attr($teacher_details);}?></textarea>
			</td>
		</tr>
		<!-- End Teacher/Speaker Details -->

		<tr valign="top">
			<th scope="row">
				<label for="teacher_fb_url"><?php esc_html_e('Facebook URL', 'bizness')?></label>
			</th>
			<td style="vertical-align: middle;">
				<input type="text" name="teacher_fb_url" class="widefat" value="<?php if(!empty($teacher_fb_url)){echo esc_url($teacher_fb_url);}?>">
			</td>
		</tr>
		<!-- End Teacher/Speaker Facebook Profile URL-->			

		<tr valign="top">
			<th scope="row">
				<label for="teacher_twitter_url"><?php esc_html_e('Twitter URL', 'bizness')?></label>
			</th>
			<td style="vertical-align: middle;">
				<input type="text" name="teacher_twitter_url" class="widefat" value="<?php if(!empty($teacher_twitter_url)){echo esc_url($teacher_twitter_url);}?>">
			</td>
		</tr>
		<!-- End Teacher/Speaker Twitter URL-->		

		<tr valign="top">
			<th scope="row">
				<label for="teacher_dribbble_url"><?php esc_html_e('Dribdble URL', 'bizness')?></label>
			</th>
			<td style="vertical-align: middle;">
				<input type="text" name="teacher_dribbble_url" class="widefat" value="<?php if(!empty($teacher_dribbble_url)){echo esc_url($teacher_dribbble_url);}?>">
			</td>
		</tr>
		<!-- End Teacher/Speaker Dribdble URL-->								

	</table>
</div>					
<?php	
}

#-----------------------------------------------------------------
# Save Service Meta
#-----------------------------------------------------------------
function save_service_meta($post_id){

	# Doing autosave then return.
	$is_autosave = wp_is_post_autosave( $post_id );
	$is_revision = wp_is_post_revision( $post_id );
	$is_valid_nonce = ( isset( $_POST[ 'bizness_meta_box_nonce' ] ) && wp_verify_nonce( $_POST[ 'bizness_meta_box_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

	#Value check and saves if needed
	if( isset( $_POST[ 'teacher_label' ] ) ) {
	    update_post_meta( $post_id, 'teacher_label', sanitize_text_field( $_POST['teacher_label'] ) );
	}		

	#Value check and saves if needed
	if( isset( $_POST[ 'teacher_name' ] ) ) {
	    update_post_meta( $post_id, 'teacher_name', sanitize_text_field( $_POST['teacher_name'] ) );
	}		

	#Value check and saves if needed
	if( isset( $_POST[ 'teacher_image_url' ] ) ) {
	    update_post_meta( $post_id, 'teacher_image_url', sanitize_text_field( $_POST['teacher_image_url'] ) );
	}		

	#Value check and saves if needed
	if( isset( $_POST[ 'teacher_designation' ] ) ) {
	    update_post_meta( $post_id, 'teacher_designation', sanitize_text_field( $_POST['teacher_designation'] ) );
	}	

	#Value check and saves if needed
	if( isset( $_POST[ 'teacher_details' ] ) ) {
	    update_post_meta( $post_id, 'teacher_details', sanitize_text_field( $_POST['teacher_details'] ) );
	}	

	#Value check and saves if needed
	if( isset( $_POST[ 'teacher_fb_url' ] ) ) {
	    update_post_meta( $post_id, 'teacher_fb_url', sanitize_text_field( $_POST['teacher_fb_url'] ) );
	}	

	#Value check and saves if needed
	if( isset( $_POST[ 'teacher_twitter_url' ] ) ) {
	    update_post_meta( $post_id, 'teacher_twitter_url', sanitize_text_field( $_POST['teacher_twitter_url'] ) );
	}	

	#Value check and saves if needed
	if( isset( $_POST[ 'teacher_dribbble_url' ] ) ) {
	    update_post_meta( $post_id, 'teacher_dribbble_url', sanitize_text_field( $_POST['teacher_dribbble_url'] ) );
	}	

}
add_action( 'save_post', 'save_service_meta' );

#-----------------------------------------------------------------
# Gallery
#-----------------------------------------------------------------
function bizness_gallery() {		

	# Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Gallery', 'Post Type General Name' ),
		'singular_name'       => _x( 'Gallery', 'Post Type Singular Name' ),
		'menu_name'           => __( 'Gallery' ),
		'parent_item_colon'   => __( 'Parent Gallery' ),
		'all_items'           => __( 'All Images' ),
		'view_item'           => __( 'View Gallery' ),
		'add_new_item'        => __( 'Add New Image' ),
		'add_new'             => __( 'Add Image ' ),
		'edit_item'           => __( 'Edit Image' ),
		'update_item'         => __( 'Update Image' ),
		'search_items'        => __( 'Search Image' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' )
	);
		
	# Set other options for Custom Post Type...
	$args = array(
		'labels'              => $labels,
		'label'               => __( 'gallery' ),
		'description'         => __( 'Gallery news and reviews' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'canesc_html_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'supports'            => array( 'title','thumbnail' ),
		'menu_icon'		      => 'dashicons-format-gallery'	
	);
	register_post_type( 'bizness-gallery', $args );
	
}
add_action( 'init', 'bizness_gallery', 0 );

function bizness_gallery_categories_taxonomies()
{
	 register_taxonomy( 'gallery_cat', 'bizness-gallery', array(
	  'hierarchical' => true,
	  'label' => 'Categories',
	  'query_var' => true,
	  'rewrite' => true
	 ));
}
 
add_action( 'init', 'bizness_gallery_categories_taxonomies', 0);


#-----------------------------------------------------------------
# FAQ
#-----------------------------------------------------------------
function bizness_faq() {		

	# Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'FAQ', 'Post Type General Name' ),
		'singular_name'       => _x( 'FAQ', 'Post Type Singular Name' ),
		'menu_name'           => __( 'FAQ' ),
		'parent_item_colon'   => __( 'Parent FAQ' ),
		'all_items'           => __( 'All FAQs' ),
		'view_item'           => __( 'View FAQ' ),
		'add_new_item'        => __( 'Add New FAQ' ),
		'add_new'             => __( 'Add FAQ ' ),
		'edit_item'           => __( 'Edit FAQ' ),
		'update_item'         => __( 'Update FAQ' ),
		'search_items'        => __( 'Search FAQ' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' )
	);
		
	# Set other options for Custom Post Type...
	$args = array(
		'labels'              => $labels,
		'label'               => __( 'faq' ),
		'description'         => __( 'FAQ news and reviews' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'canesc_html_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'supports'            => array( 'title','editor', 'revisions' ),
		'menu_icon'		      => 'dashicons-media-text'	
	);
	register_post_type( 'bizness-faq', $args );
	
}
add_action( 'init', 'bizness_faq', 0 );

#-----------------------------------------------------------------
# Team
#-----------------------------------------------------------------
function bizness_team() {		

	# Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Team', 'Post Type General Name' ),
		'singular_name'       => _x( 'Team', 'Post Type Singular Name' ),
		'menu_name'           => __( 'Team' ),
		'parent_item_colon'   => __( 'Parent Team' ),
		'all_items'           => __( 'All Team Members' ),
		'view_item'           => __( 'View Team' ),
		'add_new_item'        => __( 'Add New Team Member' ),
		'add_new'             => __( 'Add Team Member ' ),
		'edit_item'           => __( 'Edit Team Member' ),
		'update_item'         => __( 'Update Team Member' ),
		'search_items'        => __( 'Search Team Members' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' )
	);
		
	# Set other options for Custom Post Type...
	$args = array(
		'labels'              => $labels,
		'label'               => __( 'team' ),
		'description'         => __( 'Team news and reviews' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'canesc_html_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'supports'            => array( 'title','editor', 'thumbnail', 'revisions' ),
		'menu_icon'		      => 'dashicons-admin-users'	
	);
	register_post_type( 'bizness-team', $args );
	
}
add_action( 'init', 'bizness_team', 0 );

function register_bizness_team_meta() {
    add_meta_box( 
        'bizness_team_meta_id',                           					# Metabox
        __( 'Details', 'bizness' ),           								# Title
        'team_meta',                               						# Call Back func
       	'bizness-team',                                         			# post type
        'normal'                                            			# Text Content
    );                                                  				# Text Content  
}
add_action( 'add_meta_boxes', 'register_bizness_team_meta' );


#-----------------------------------------------------------------
# Team Meta
#-----------------------------------------------------------------
function team_meta( $post, $args ){

	wp_nonce_field( basename( __FILE__ ), 'bizness_meta_box_nonce' );

	$team_designation			= get_post_meta($post->ID, 'team_designation', true);
	$team_fb					= get_post_meta($post->ID, 'team_fb', true);
	$team_twitter				= get_post_meta($post->ID, 'team_twitter', true);
	$team_dribbble				= get_post_meta($post->ID, 'team_dribbble', true);
	$team_instagram				= get_post_meta($post->ID, 'team_instagram', true);

?>
<div class="wrap">
	<table class="form-table">
		<tr valign="top">
			<th scope="row">
				<label for="team_designation"><?php esc_html_e('Designation', 'bizness')?></label>
			</th>
			<td style="vertical-align: middle;">
				<input type="text" name="team_designation" id="team_designation" class="widefat" value="<?php if( !empty( $team_designation) ){ echo esc_attr($team_designation); } ?>">
			</td>
		</tr>
		<!-- End Designation -->

		<tr valign="top">
			<th scope="row">
				<label for="team_fb"><?php esc_html_e('Facebook Profile URL', 'bizness')?></label>
			</th>
			<td style="vertical-align: middle;">
				<input type="text" name="team_fb" id="team_fb" class="widefat" value="<?php if( !empty( $team_fb) ){ echo esc_url($team_fb); } ?>">
			</td>
		</tr>
		<!-- End Facebook Profile URL -->		

		<tr valign="top">
			<th scope="row">
				<label for="team_twitter"><?php esc_html_e('Twitter Profile URL', 'bizness')?></label>
			</th>
			<td style="vertical-align: middle;">
				<input type="text" name="team_twitter" id="team_twitter" class="widefat" value="<?php if( !empty( $team_twitter) ){ echo esc_url($team_twitter); } ?>">
			</td>
		</tr>
		<!-- End Twitter Profile URL -->			

		<tr valign="top">
			<th scope="row">
				<label for="team_dribbble"><?php esc_html_e('Dibbble Profile URL', 'bizness')?></label>
			</th>
			<td style="vertical-align: middle;">
				<input type="text" name="team_dribbble" id="team_dribbble" class="widefat" value="<?php if( !empty( $team_dribbble) ){ echo esc_url($team_dribbble); } ?>">
			</td>
		</tr>
		<!-- End Dibbble Profile URL -->		

		<tr valign="top">
			<th scope="row">
				<label for="team_instagram"><?php esc_html_e('Instagram Profile URL', 'bizness')?></label>
			</th>
			<td style="vertical-align: middle;">
				<input type="text" name="team_instagram" id="team_instagram" class="widefat" value="<?php if( !empty($team_instagram) ){ echo esc_url($team_instagram); } ?>">
			</td>
		</tr>
		<!-- End Instagram Profile URL -->		

	</table>
</div>					
<?php	
}

#-----------------------------------------------------------------
# Save Team Meta
#-----------------------------------------------------------------
function save_team_meta( $post_id ){

	# Doing autosave then return.
	$is_autosave = wp_is_post_autosave( $post_id );
	$is_revision = wp_is_post_revision( $post_id );
	$is_valid_nonce = ( isset( $_POST[ 'bizness_meta_box_nonce' ] ) && wp_verify_nonce( $_POST[ 'bizness_meta_box_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

	#Value check and saves if needed
	if( isset( $_POST[ 'team_designation' ] ) ) {
	    update_post_meta( $post_id, 'team_designation', sanitize_text_field( $_POST['team_designation'] ) );
	}

	#Value check and saves if needed
	if( isset( $_POST[ 'team_fb' ] ) ) {
	    update_post_meta( $post_id, 'team_fb', esc_url( $_POST['team_fb'] ) );
	}

	#Value check and saves if needed
	if( isset( $_POST[ 'team_twitter' ] ) ) {
	    update_post_meta( $post_id, 'team_twitter', esc_url( $_POST['team_twitter'] ) );
	}

	#Value check and saves if needed
	if( isset( $_POST[ 'team_dribbble' ] ) ) {
	    update_post_meta( $post_id, 'team_dribbble', esc_url( $_POST['team_dribbble'] ) );
	}		

	#Value check and saves if needed
	if( isset( $_POST[ 'team_instagram' ] ) ) {
	    update_post_meta( $post_id, 'team_instagram', esc_url( $_POST['team_instagram'] ) );
	}			

}
add_action( 'save_post', 'save_team_meta' );

#-----------------------------------------------------------------
# Testimonials
#-----------------------------------------------------------------
function bizness_testimonial() {		

	# Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Testimonial', 'Post Type General Name' ),
		'singular_name'       => _x( 'Testimonial', 'Post Type Singular Name' ),
		'menu_name'           => __( 'Testimonial' ),
		'parent_item_colon'   => __( 'Parent Testimonial' ),
		'all_items'           => __( 'All Testimonials' ),
		'view_item'           => __( 'View Testimonials' ),
		'add_new_item'        => __( 'Add New Testimonial' ),
		'add_new'             => __( 'Add Testimonial ' ),
		'edit_item'           => __( 'Edit Testimonial' ),
		'update_item'         => __( 'Update Testimonial' ),
		'search_items'        => __( 'Search Testimonials' ),
		'not_found'           => __( 'Not Found' ),
		'not_found_in_trash'  => __( 'Not found in Trash' )
	);
		
	# Set other options for Custom Post Type...
	$args = array(
		'labels'              => $labels,
		'label'               => __( 'testimonial' ),
		'description'         => __( 'Testimonial news and reviews' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'canesc_html_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'supports'            => array( 'title','editor', 'thumbnail', 'revisions' ),
		'menu_icon'		      => 'dashicons-format-quote'	
	);
	register_post_type( 'bizness-testimonial', $args );
	
}
add_action( 'init', 'bizness_testimonial', 0 );

function register_bizness_testimonial_meta() {
    add_meta_box( 
        'bizness_testimonial_meta_id',                           			# Metabox
        __( 'Details', 'bizness' ),           								# Title
        'testimonial_meta',                               			    # Call Back func
       	'bizness-testimonial',                                         	# post type
        'normal'                                            			# Text Content
    );                                                  				# Text Content  
}
add_action( 'add_meta_boxes', 'register_bizness_testimonial_meta' );


#-----------------------------------------------------------------
# Testimonial Meta
#-----------------------------------------------------------------
function testimonial_meta( $post, $args ){

	wp_nonce_field( basename( __FILE__ ), 'bizness_meta_box_nonce' );

	$testimonial_designation			= get_post_meta($post->ID, 'testimonial_designation', true);

?>
<div class="wrap">
	<table class="form-table">

		<tr valign="top">
			<th scope="row">
				<label for="testimonial_designation"><?php esc_html_e('Client Designation', 'bizness')?></label>
			</th>
			<td style="vertical-align: middle;">
				<input type="text" name="testimonial_designation" id="testimonial_designation" class="widefat" value="<?php if( !empty( $testimonial_designation) ){ echo esc_attr($testimonial_designation); } ?>">
			</td>
		</tr>
		<!-- End Designation -->

	</table>
</div>					
<?php	
}

#-----------------------------------------------------------------
# Save Testimonial Meta
#-----------------------------------------------------------------
function save_testimonial_meta( $post_id ){

	# Doing autosave then return.
	$is_autosave = wp_is_post_autosave( $post_id );
	$is_revision = wp_is_post_revision( $post_id );
	$is_valid_nonce = ( isset( $_POST[ 'bizness_meta_box_nonce' ] ) && wp_verify_nonce( $_POST[ 'bizness_meta_box_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

	#Value check and saves if needed
	if( isset( $_POST[ 'testimonial_designation' ] ) ) {
	    update_post_meta( $post_id, 'testimonial_designation', sanitize_text_field( $_POST['testimonial_designation'] ) );
	}

}
add_action( 'save_post', 'save_testimonial_meta' );
?>