<?php
/*-----------------------------------------------------------------------------------*/
/* Start Using bizness Shortcodes in the Visual Composer */
/*-----------------------------------------------------------------------------------*/
add_action( 'init', 'bizness_vc_shortcodes' );
function bizness_vc_shortcodes() {

/******************
Video Slider
******************/
vc_map( array(
      "name" => esc_html__("MXT- Video Slider", 'bizness'),
      "base" => "bizness_video_slider",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Video Slider', 'bizness'),
      "params" => array(
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Video URL", 'bizness'),
            "param_name" => "bizness_video_url",
            "value" => '',
            "description" => ''              
         ),            
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Video Thumb URL", 'bizness'),
            "param_name" => "bizness_video_thumb_url",
            "value" => '',
            "description" => ''              
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Heading - Normal Text", 'bizness'),
            "param_name" => "bizness_heading_normalt",
            "value" => '',
            "description" => ''              
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Heading - Strong Text", 'bizness'),
            "param_name" => "bizness_heading_strongt",
            "value" => '',
            "description" => ''              
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Heading - Normal Text 2", 'bizness'),
            "param_name" => "bizness_heading_normalt2",
            "value" => '',
            "description" => ''              
         ),         
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Details Text", 'bizness'),
            "param_name" => "bizness_details",
            "value" => '',
            "description" => ''              
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Button One Text", 'bizness'),
            "param_name" => "bizness_button1_text",
            "value" => '',
            "description" => ''              
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Button One URL", 'bizness'),
            "param_name" => "bizness_button1_url",
            "value" => '',
            "description" => ''              
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Button Two Text", 'bizness'),
            "param_name" => "bizness_button2_text",
            "value" => '',
            "description" => ''              
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Button Two URL", 'bizness'),
            "param_name" => "bizness_button2_url",
            "value" => '',
            "description" => ''              
         )                                    
      )
   )
); 

/******************
Image Slider
******************/
vc_map( array(
      "name" => esc_html__("MXT- Image Slider", 'bizness'),
      "base" => "bizness_image_slider",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Image Slider', 'bizness'),
      "params" => array(
         array(
               "type" => "dropdown",            
               "class" => "",
               "heading" => esc_html__("Number of Slider", 'bizness'),
               "param_name" => "number_of_slider",
               'admin_label' => true,
               "value" => array(
                  esc_html__('One', 'bizness')=>'1',
                  esc_html__('Two', 'bizness')=>'2', 
                  esc_html__('Three', 'bizness')=>'3',
                  esc_html__('Four', 'bizness')=>'4',
                  esc_html__('Five', 'bizness')=>'5'
               )            
         ),              
         array(
            "type" => "attach_image",            
            "class" => "",
            "heading" => esc_html__("Slider 1 Image Url", 'bizness'),
            "param_name" => "bizness_img_path_1",
            'description' => esc_html__('Slider 1 Image Url', 'bizness'),
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array('1',
                                    '2',
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 1 Heading - Normal Text", 'bizness'),
            "param_name" => "bizness_heading_normalt_1",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array('1',
                                    '2',
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                          
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 1 Heading - Strong Text", 'bizness'),
            "param_name" => "bizness_heading_strongt_1",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array('1',
                                    '2',
                                    '3',
                                    '4',
                                    '5',
                                    '6',
                                    '7',
                                    '8'
                     )                  
               )                          
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 1 Heading - Normal Text 2", 'bizness'),
            "param_name" => "bizness_heading_normalt2_1",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array('1',
                                    '2',
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                          
         ),         
         array(
            "type" => "textarea",            
            "class" => "",
            "heading" => esc_html__("Slider 1 Details Text", 'bizness'),
            "param_name" => "bizness_details_1",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array('1',
                                    '2',
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                           
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 1 Button One Text", 'bizness'),
            "param_name" => "bizness_button1_text_1",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array('1',
                                    '2',
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                            
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 1 Button One URL", 'bizness'),
            "param_name" => "bizness_button1_url_1",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array('1',
                                    '2',
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                            
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 1 Button Two Text", 'bizness'),
            "param_name" => "bizness_button2_text_1",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array('1',
                                    '2',
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                            
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 1 Button Two URL", 'bizness'),
            "param_name" => "bizness_button2_url_1",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array('1',
                                    '2',
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                            
         ),

         array(
            "type" => "attach_image",            
            "class" => "",
            "heading" => esc_html__("Slider 2 Image Url", 'bizness'),
            "param_name" => "bizness_img_path_2",
            'description' => esc_html__('Slider 2 Image Url', 'bizness'),
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '2',
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 2 Heading - Normal Text", 'bizness'),
            "param_name" => "bizness_heading_normalt_2",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '2',
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                          
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 2 Heading - Strong Text", 'bizness'),
            "param_name" => "bizness_heading_strongt_2",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '2',
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                          
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 2 Heading - Normal Text 2", 'bizness'),
            "param_name" => "bizness_heading_normalt2_2",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '2',
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                          
         ),         
         array(
            "type" => "textarea",            
            "class" => "",
            "heading" => esc_html__("Slider 2 Details Text", 'bizness'),
            "param_name" => "bizness_details_2",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '2',
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                           
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 2 Button One Text", 'bizness'),
            "param_name" => "bizness_button1_text_2",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '2',
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                            
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 2 Button One URL", 'bizness'),
            "param_name" => "bizness_button1_url_2",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '2',
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                            
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 2 Button Two Text", 'bizness'),
            "param_name" => "bizness_button2_text_2",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '2',
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                            
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 2 Button Two URL", 'bizness'),
            "param_name" => "bizness_button2_url_2",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '2',
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                            
         ),
         array(
            "type" => "attach_image",            
            "class" => "",
            "heading" => esc_html__("Slider 3 Image Url", 'bizness'),
            "param_name" => "bizness_img_path_3",
            'description' => esc_html__('', 'bizness'),
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 3 Heading - Normal Text", 'bizness'),
            "param_name" => "bizness_heading_normalt_3",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                          
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 3 Heading - Strong Text", 'bizness'),
            "param_name" => "bizness_heading_strongt_3",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                          
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 3 Heading - Normal Text 2", 'bizness'),
            "param_name" => "bizness_heading_normalt2_3",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                          
         ),         
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 3 Details Text", 'bizness'),
            "param_name" => "bizness_details_3",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                           
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 3 Button One Text", 'bizness'),
            "param_name" => "bizness_button1_text_3",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                            
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 3 Button One URL", 'bizness'),
            "param_name" => "bizness_button1_url_3",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                            
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 3 Button Two Text", 'bizness'),
            "param_name" => "bizness_button2_text_3",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                            
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 3 Button Two URL", 'bizness'),
            "param_name" => "bizness_button2_url_3",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '3',
                                    '4',
                                    '5'
                     )                  
               )                            
         ),
         array(
            "type" => "attach_image",            
            "class" => "",
            "heading" => esc_html__("Slider 4 Image Url", 'bizness'),
            "param_name" => "bizness_img_path_4",
            'description' => esc_html__('Slider 4 Image Url', 'bizness'),
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '4',
                                    '5'
                     )                  
               )
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 4 Heading - Normal Text", 'bizness'),
            "param_name" => "bizness_heading_normalt_4",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '4',
                                    '5'
                     )                  
               )                          
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 4 Heading - Strong Text", 'bizness'),
            "param_name" => "bizness_heading_strongt_4",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '4',
                                    '5'
                     )                  
               )                          
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 4 Heading - Normal Text 2", 'bizness'),
            "param_name" => "bizness_heading_normalt2_4",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '4',
                                    '5'
                     )                  
               )                          
         ),         
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 4 Details Text", 'bizness'),
            "param_name" => "bizness_details_4",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '4',
                                    '5'
                     )                  
               )                           
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 4 Button One Text", 'bizness'),
            "param_name" => "bizness_button1_text_4",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '4',
                                    '5'
                     )                  
               )                            
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 4 Button One URL", 'bizness'),
            "param_name" => "bizness_button1_url_4",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '4',
                                    '5'
                     )                  
               )                            
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 4 Button Two Text", 'bizness'),
            "param_name" => "bizness_button2_text_4",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '4',
                                    '5'
                     )                  
               )                            
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 4 Button Two URL", 'bizness'),
            "param_name" => "bizness_button2_url_4",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '4',
                                    '5'
                     )                  
               )                            
         ),

         array(
            "type" => "attach_image",            
            "class" => "",
            "heading" => esc_html__("Slider 5 Image Url", 'bizness'),
            "param_name" => "bizness_img_path_5",
            'description' => esc_html__('Slider 5 Image Url', 'bizness'),
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '5'
                     )                  
               )
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 5 Heading - Normal Text", 'bizness'),
            "param_name" => "bizness_heading_normalt_5",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '5'
                     )                  
               )                          
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 5 Heading - Strong Text", 'bizness'),
            "param_name" => "bizness_heading_strongt_5",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '5'
                     )                  
               )                          
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 5 Heading - Normal Text 2", 'bizness'),
            "param_name" => "bizness_heading_normalt2_5",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '5'
                     )                  
               )                          
         ),         
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 5 Details Text", 'bizness'),
            "param_name" => "bizness_details_5",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '5'
                     )                  
               )                           
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 5 Button One Text", 'bizness'),
            "param_name" => "bizness_button1_text_5",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '5'
                     )                  
               )                            
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 5 Button One URL", 'bizness'),
            "param_name" => "bizness_button1_url_5",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '5'
                     )                  
               )                            
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 5 Button Two Text", 'bizness'),
            "param_name" => "bizness_button2_text_5",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '5'
                     )                  
               )                            
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Slider 5 Button Two URL", 'bizness'),
            "param_name" => "bizness_button2_url_5",
            "value" => '',
            "description" => '',
            'dependency' => array(
                  'element'=>'number_of_slider',
                  'value' => array(
                                    '5'
                     )                  
               )                            
         )
      )
   )
);  

/******************
Heading
******************/
vc_map( array(
      "name" => esc_html__("MXT- Heading", 'bizness'),
      "base" => "bizness_common_heading",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Heading', 'bizness'),
      "params" => array(
            array(
               "type" => "textfield",            
               "class" => "",
               "heading" => esc_html__("Heading - Normal Text", 'bizness'),
               "param_name" => "bizness_heading_normalt",
               "value" => '',
               "description" => ''              
            ),            
            array(
               "type" => "textfield",            
               "class" => "",
               "heading" => esc_html__("Heading - Strong Text", 'bizness'),
               "param_name" => "bizness_heading_strongt",
               "value" => '',
               "description" => ''              
            ),
            array(
               "type" => "textfield",            
               "class" => "",
               "heading" => esc_html__("Details Text", 'bizness'),
               "param_name" => "bizness_subheading",
               "value" => '',
               "description" => ''              
            )
      )
   )
);

/******************
Left Align Heading
******************/
vc_map( array(
      "name" => esc_html__("MXT- Left Align Heading", 'bizness'),
      "base" => "bizness_left_align_heading",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Left Align Heading', 'bizness'),
      "params" => array(
            array(
               "type" => "textfield",            
               "class" => "",
               "heading" => esc_html__("Heading - Normal Text", 'bizness'),
               "param_name" => "bizness_heading_normalt",
               "value" => '',
               "description" => ''              
            ),            
            array(
               "type" => "textfield",            
               "class" => "",
               "heading" => esc_html__("Heading - Strong Text", 'bizness'),
               "param_name" => "bizness_heading_strongt",
               "value" => '',
               "description" => ''              
            )
      )
   )
);

/******************
Services
******************/
vc_map( array(
      "name" => esc_html__("MXT- Services", 'bizness'),
      "base" => "bizness_services",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Services', 'bizness'),
      "params" => array(
            array(
               "type" => "textfield",            
               "class" => "",
               "heading" => esc_html__("Heading", 'bizness'),
               "param_name" => "bizness_heading",
               "value" => '',
               "description" => ''              
            ),            
            array(
               "type" => "textfield",            
               "class" => "",
               "heading" => esc_html__("Icon", 'bizness'),
               "param_name" => "bizness_icon",
               "value" => '',
               "description" => 'Insert Icon name (e.g. icon-bars, icon-target, icon-global, icon-globe, icon-layers, icon-laptop, fa fa-user, fa fa-mobile )'              
            ),
            array(
               "type" => "textarea",            
               "class" => "",
               "heading" => esc_html__("Service Details", 'bizness'),
               "param_name" => "bizness_details",
               "value" => '',
               "description" => ''              
            ),            
            array(
               "type" => "colorpicker",            
               "class" => "",
               "heading" => esc_html__("Background Hover Color", 'bizness'),
               "param_name" => "bizness_hcolor",
               "value" => '',
               "description" => ''              
            )
      )
   )
);

/******************
Call To Action
******************/
vc_map( array(
      "name" => esc_html__("MXT- Call To Action", 'bizness'),
      "base" => "bizness_call_to_action",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Call To Action', 'bizness'),
      "params" => array(
            array(
               "type" => "textfield",            
               "class" => "",
               "heading" => esc_html__("Heading", 'bizness'),
               "param_name" => "bizness_heading",
               "value" => '',
               "description" => ''              
            ),              
            array(
               "type" => "textfield",            
               "class" => "",
               "heading" => esc_html__("Sub Heading", 'bizness'),
               "param_name" => "bizness_subheading",
               "value" => '',
               "description" => ''              
            ),              
            array(
               "type" => "attach_image",            
               "class" => "",
               "heading" => esc_html__("Background Image URL", 'bizness'),
               "param_name" => "bizness_bgimg_url",
               "value" => '',
               "description" => ''              
            ),             
            array(
               "type" => "textfield",            
               "class" => "",
               "heading" => esc_html__("Button Text", 'bizness'),
               "param_name" => "bizness_btn_text",
               "value" => '',
               "description" => ''              
            ),             
            array(
               "type" => "textfield",            
               "class" => "",
               "heading" => esc_html__("Button URL", 'bizness'),
               "param_name" => "bizness_btn_url",
               "value" => '',
               "description" => ''              
            )
      )
   )
);

/******************
Gallery
******************/
vc_map( array(
      "name" => esc_html__("MXT- Gallery/Portfolio", 'bizness'),
      "base" => "bizness_gallery_portfolio",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Gallery', 'bizness'),
      "params" => array(
            array(
               "type" => "textfield",            
               "class" => "",
               "heading" => esc_html__("Heading - Normal Text", 'bizness'),
               "param_name" => "bizness_heading_normalt",
               "value" => '',
               "description" => ''              
            ),            
            array(
               "type" => "textfield",            
               "class" => "",
               "heading" => esc_html__("Heading - Strong Text", 'bizness'),
               "param_name" => "bizness_heading_strongt",
               "value" => '',
               "description" => ''              
            )
      )
   )
);

/************
Stats Section
************/
vc_map( array(
      "name" => esc_html__("MXT- Stats Section", 'bizness'),
      "base" => "bizness_shortcodes_stats",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Stats Section', 'bizness'),
      "params" => array(
         array(
            "type" => "attach_image",            
            "class" => "",
            "heading" => esc_html__("Backgroun Image URL", 'bizness'),
            "param_name" => "stat_bgimg_url",
            "value" => '',
            "description" => '',
         ),         
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("1st Stat Title", 'bizness'),
            "param_name" => "stat_title1",
            "value" => '',
            "description" => 'E.g Project Completed',
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("1st Stat Icon", 'bizness'),
            "param_name" => "stat_icon1",
            "value" => '',
            "description" => esc_html__('Insert icon. E.g icon-layers, icon-trophy, icon-icons20, icon-happy, fa fa-user', 'bizness'),
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("1st Stat Number", 'bizness'),
            "param_name" => "stat_number1",
            "value" => '',
            "description" => esc_html__('Example: 1235','bizness')
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("2nd Stat Title", 'bizness'),
            "param_name" => "stat_title2",
            "value" => '',
            "description" => 'E.g Awards Won',
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("2nd Stat Icon", 'bizness'),
            "param_name" => "stat_icon2",
            "value" => '',
            "description" => esc_html__('Insert icon. E.g icon-layers, icon-trophy, icon-icons20, icon-happy, fa fa-user', 'bizness'),
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("2nd Stat Number", 'bizness'),
            "param_name" => "stat_number2",
            "value" => '',
            "description" => esc_html__('Example: 78','bizness')
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("3rd Stat Title", 'bizness'),
            "param_name" => "stat_title3",
            "value" => '',
            "description" => 'E.g Hours of Work / Month',
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("3rd Stat Icon", 'bizness'),
            "param_name" => "stat_icon3",
            "value" => '',
            "description" => esc_html__('Insert icon. E.g icon-layers, icon-trophy, icon-icons20, icon-happy, fa fa-user', 'bizness'),
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("3rd Stat Number", 'bizness'),
            "param_name" => "stat_number3",
            "value" => '',
            "description" => esc_html__('Example: 186','bizness')
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("4th Stat Title", 'bizness'),
            "param_name" => "stat_title4",
            "value" => '',
            "description" => 'E.g Satisfied Clients',
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("4th Stat Icon", 'bizness'),
            "param_name" => "stat_icon4",
            "value" => '',
            "description" => esc_html__('Insert icon. E.g icon-layers, icon-trophy, icon-icons20, icon-happy, fa fa-user', 'bizness'),
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("4th Stat Number", 'bizness'),
            "param_name" => "stat_number4",
            "value" => '',
            "description" => esc_html__('Example: 89','bizness')
         )
      )
   )
);

/************
Testimonials
************/
vc_map( array(
      "name" => esc_html__("MXT- Testimonials", 'bizness'),
      "base" => "bizness_testimonials",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Testimonials', 'bizness'),
      "params" => array(        
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Heading - Normal Text", 'bizness'),
            "param_name" => "bizness_heading_normalt",
            "value" => '',
            "description" => ''              
         ),            
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Heading - Strong Text", 'bizness'),
            "param_name" => "bizness_heading_strongt",
            "value" => '',
            "description" => ''              
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Display Limits", 'bizness'),
            "param_name" => "bizness_display_limits",
            "value" => '3',
            "description" => ''              
         ),         
         array(
            "type" => "dropdown",            
            "class" => "",
            "heading" => esc_html__("Display Order", 'bizness'),
            "param_name" => "bizness_display_order",
            "value" => array(
               esc_html__('DESC', 'bizness')=>'DESC',
               esc_html__('ASC', 'bizness')=>'ASC'
            )              
         )
      )
   )
);

/************
Pricing Tables
************/
vc_map( array(
      "name" => esc_html__("MXT- Pricing Tables", 'bizness'),
      "base" => "bizness_pricing_tables",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Pricing Tables', 'bizness'),
      "params" => array(        
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Heading", 'bizness'),
            "param_name" => "bizness_heading",
            "value" => '',
            "description" => 'E.g: Basic'              
         ),  
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Currency Symbol", 'bizness'),
            "param_name" => "bizness_currency_symbol",
            "value" => '',
            "description" => 'If you leave it as blank, $ currency will display. E.g $'              
         ),       
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Price", 'bizness'),
            "param_name" => "bizness_price",
            "value" => '',
            "description" => 'E.g: 9.99'              
         ),
         array(
            "type" => "textarea",            
            "class" => "",
            "heading" => esc_html__("Short Description", 'bizness'),
            "param_name" => "bizness_description",
            "value" => '',
            "description" => ''              
         ),         
         array(
            "type" => "textarea",            
            "class" => "",
            "heading" => esc_html__("Bullet Texts", 'bizness'),
            "param_name" => "bizness_bullet_text",
            "description" => 'Start each point on new line'                       
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Button Text", 'bizness'),
            "param_name" => "bizness_button_text",
            "value" => '',
            "description" => 'E.g Choose Plan'              
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Button URL", 'bizness'),
            "param_name" => "bizness_button_url",
            "value" => '',
            "description" => ''              
         ), 
         array(
            "type" => "checkbox",            
            "class" => "",
            "heading" => esc_html__("Set Active", 'bizness'),
            "param_name" => "bizness_isactive",
            "value" => array(
                 "" => "false"
               ),
            "description" => 'Set it active'               
         ),                              
      )
   )
);

/************
Team
************/
vc_map( array(
      "name" => esc_html__("MXT- Team", 'bizness'),
      "base" => "bizness_team",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Team', 'bizness'),
      "params" => array(  
         array(
            "type" => "attach_image",            
            "class" => "",
            "heading" => esc_html__("Background Image URL", 'bizness'),
            "param_name" => "bizness_bgimg_url",
            "value" => '',
            "description" => ''              
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Heading - Normal Text", 'bizness'),
            "param_name" => "bizness_heading_normalt",
            "value" => '',
            "description" => ''              
         ),            
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Heading - Strong Text", 'bizness'),
            "param_name" => "bizness_heading_strongt",
            "value" => '',
            "description" => ''              
         ),         
         array(
            "type" => "dropdown",            
            "class" => "",
            "heading" => esc_html__("Display Order", 'bizness'),
            "param_name" => "bizness_display_order",
            "value" => array(
               esc_html__('DESC', 'bizness')=>'DESC',
               esc_html__('ASC', 'bizness')=>'ASC'
            )              
         ),
         array(
            "type" => "colorpicker",            
            "class" => "",
            "heading" => esc_html__("Name Text Color", 'bizness'),
            "param_name" => "bizness_nametext_color",           
         ),         
         array(
            "type" => "colorpicker",            
            "class" => "",
            "heading" => esc_html__("Details Text Color", 'bizness'),
            "param_name" => "bizness_detailstext_color",           
         ),
      )
   )
);

/************
Recent News
************/
function bizness_post_categories(){

   $cat_args=  array(
       'order'                    => 'ASC',
       'orderby'                  => 'id',
       'hierarchical'             => 0,
       'hide_empty'               => 0,
   );   

   $output_categories = array();
   $categories=get_categories($cat_args);
     foreach($categories as $category) { 
        $output_categories[$category->name] = $category->cat_ID;
   }

   return $output_categories;
}

vc_map( array(
      "name" => esc_html__("MXT- Recent News", 'bizness'),
      "base" => "bizness_recent_news",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Recent News', 'bizness'),
      "params" => array(
         array(
            "type" => "dropdown",            
            "class" => "",
            "heading" => esc_html__("Layout ", 'bizness'),
            "param_name" => "bizness_display_layout",
            "value" => array(
               esc_html__('Style 1', 'bizness')=> '1',
               esc_html__('Style 2', 'bizness')=> '2'
            )           
         ),          
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Heading - Normal Text", 'bizness'),
            "param_name" => "bizness_heading_normalt",
            "value" => '',
            "description" => 'E.g: Latest'              
         ),            
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Heading - Strong Text", 'bizness'),
            "param_name" => "bizness_heading_strongt",
            "value" => '',
            "description" => 'E.g: News'              
         ),        
         array(
            "type" => "dropdown",            
            "class" => "",
            "heading" => esc_html__("Category", 'bizness'),
            "param_name" => "bizness_post_category",
            "value" => bizness_post_categories(),
            "description" => ''              
         ),             
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Post Limits", 'bizness'),
            "param_name" => "bizness_post_limits",
            "value" => '3',
            "description" => ''              
         ),                    
         array(
            "type" => "dropdown",            
            "class" => "",
            "heading" => esc_html__("Display Order", 'bizness'),
            "param_name" => "bizness_display_order",
            "value" => array(
               esc_html__('DESC', 'bizness')=>'DESC',
               esc_html__('ASC', 'bizness')=>'ASC'
            )              
         )
      )
   )
);

/************
Contact Address
************/
vc_map( array(
      "name" => esc_html__("MXT- Contact Address", 'bizness'),
      "base" => "bizness_contact_address",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Contact Address', 'bizness'),
      "params" => array(
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Heading - Normal Text", 'bizness'),
            "param_name" => "bizness_heading_normalt",
            "value" => '',
            "description" => 'E.g: Get'              
         ),            
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Heading - Strong Text", 'bizness'),
            "param_name" => "bizness_heading_strongt",
            "value" => '',
            "description" => 'E.g:in Touch
'              
         ),                     
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Sub Heading", 'bizness'),
            "param_name" => "bizness_subheading",
            "value" => '',
            "description" => ''              
         ), 
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Location", 'bizness'),
            "param_name" => "bizness_location",
            "value" => '',
            "description" => ''              
         ), 
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Eamil", 'bizness'),
            "param_name" => "bizness_email",
            "value" => '',
            "description" => ''              
         ),                                     
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Contact No", 'bizness'),
            "param_name" => "bizness_contactno",
            "value" => '',
            "description" => ''              
         ),                   
      )
   )
);

/************
Map
************/
vc_map( array(
      "name" => esc_html__("MXT- Map", 'bizness'),
      "base" => "bizness_map",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Map', 'bizness'),
      "params" => array(
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Latitude and Longitude", 'bizness'),
            "param_name" => "bizness_lat_long",
            "value" => '',
            "description" => 'Latitude and Longitude with comma seperator. E.g: 51.507309, -0.127448'              
         )                  
      )
   )
);

/************
Welcome Note
************/
vc_map( array(
      "name" => esc_html__("MXT- Welcome Note", 'bizness'),
      "base" => "bizness_welcome_note",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Welcome Note', 'bizness'),
      "params" => array(
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Heading - Normal Text", 'bizness'),
            "param_name" => "bizness_heading_normalt",
            "value" => '',
            "description" => ''              
         ),            
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Heading - Strong Text", 'bizness'),
            "param_name" => "bizness_heading_strongt",
            "value" => '',
            "description" => ''              
         ),         
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Sub Heading", 'bizness'),
            "param_name" => "bizness_subheading",
            "value" => '',
            "description" => ''              
         ),                                     
         array(
            "type" => "textarea_html",            
            "class" => "",
            "heading" => esc_html__("Description", 'bizness'),
            "param_name" => "bizness_deatils",
            "value" => '',
            "description" => ''              
         )                 
      )
   )
);

/************
Featured Image
************/
vc_map( array(
      "name" => esc_html__("MXT- Featured Image", 'bizness'),
      "base" => "bizness_featured_image",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Featured Image', 'bizness'),
      "params" => array(
         array(
            "type" => "attach_image",            
            "class" => "",
            "heading" => esc_html__("Image", 'bizness'),
            "param_name" => "bizness_img",
            "value" => '',
            "description" => ''              
         )               
      )
   )
);

/************
Content Box
************/
vc_map( array(
      "name" => esc_html__("MXT- Content Box", 'bizness'),
      "base" => "bizness_content_box",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Content Box', 'bizness'),
      "params" => array(
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Green Color Text", 'bizness'),
            "param_name" => "bizness_green_text",
            "value" => '',
            "description" => ''                          
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Heading", 'bizness'),
            "param_name" => "bizness_heading",
            "value" => '',
            "description" => '',                           
         ),         
         array(
            "type" => "textarea",            
            "class" => "",
            "heading" => esc_html__("Description", 'bizness'),
            "param_name" => "bizness_details",
            "value" => '',
            "description" => ''                        
         ),          
         array(
            "type" => "attach_image",            
            "class" => "",
            "heading" => esc_html__("Thumbnail Image", 'bizness'),
            "param_name" => "bizness_img",
            "value" => '',
            "description" => ''                          
         )
      )
   )
);

/************
About Company
************/
vc_map( array(
      "name" => esc_html__("MXT- About Company", 'bizness'),
      "base" => "bizness_about_company",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('About Company', 'bizness'),
      "params" => array(
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Heading - Normal Text", 'bizness'),
            "param_name" => "bizness_heading_normalt",
            "value" => '',
            "description" => ''              
         ),            
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Heading - Strong Text", 'bizness'),
            "param_name" => "bizness_heading_strongt",
            "value" => '',
            "description" => ''              
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Sub Heading", 'bizness'),
            "param_name" => "bizness_subheading",
            "value" => '',
            "description" => '',                           
         ),         
         array(
            "type" => "textarea",            
            "class" => "",
            "heading" => esc_html__("Description", 'bizness'),
            "param_name" => "bizness_details",
            "value" => '',
            "description" => '',                         
         ),
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Button Text", 'bizness'),
            "param_name" => "bizness_button_text",
            "value" => '',
            "description" => 'E.g Read More',                           
         ),         
         array(
            "type" => "textfield",            
            "class" => "",
            "heading" => esc_html__("Button URL", 'bizness'),
            "param_name" => "bizness_button_url",
            "value" => '',
            "description" => '',                           
         ),
         array(
            "type" => "attach_image",            
            "class" => "",
            "heading" => esc_html__("Image", 'bizness'),
            "param_name" => "bizness_img",
            "value" => '',
            "description" => ''              
         )                     
      )
   )
);

/******************
Thumbnail Image Gallery
******************/
function bizness_thumb_gallery(){
   $taxonomy_array = array();
   $taxonomies = array(
   'taxonomy' => 'gallery_cat'
   );
   $taxonomy_terms = get_terms( $taxonomies );
   foreach ( $taxonomy_terms as $taxonomy_term ) :
       $taxonomy_term_name = $taxonomy_term->name;
       $taxonomy_term_id = $taxonomy_term->term_id;
       $taxonomy_array[ $taxonomy_term_name ] = $taxonomy_term_id;
   endforeach;
   return  $taxonomy_array;
}

vc_map( array(
      "name" => esc_html__("MXT- Thumbnail Image Row", 'bizness'),
      "base" => "bizness_thumbnail_image_row",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Thumbnail Image Row', 'bizness'),
      "params" => array(
            array(
               "type" => "dropdown",            
               "class" => "",
               "heading" => esc_html__("Categories", 'bizness'),
               "param_name" => "bizness_gallery_cat",
               "value" => bizness_thumb_gallery(),
               "description" => ''              
            ),
            array(
               "type" => "textfield",            
               "class" => "",
               "heading" => esc_html__("Image Limits", 'bizness'),
               "param_name" => "bizness_image_limits",
               "value" => '5',
               "description" => ''              
            )
      )
   )
);

/******************
Menu 
******************/

function get_all_menu(){

   $menus = get_terms('nav_menu');
   $bizness_menu = array();
   foreach($menus as $menu){
      $bizness_menu[$menu->name] = $menu->term_id;
   }

   return $bizness_menu;
}
vc_map( array(
      "name" => esc_html__("FT- Menu ", 'bizness'),
      "base" => "bizness_menu",
      "icon" => plugins_url( 'images/vc.png', __FILE__ ),
      "class" => "",
      "category" => esc_html__('Max-Themes', 'bizness'),
      'admin_enqueue_js' => '',
      'admin_enqueue_css' => '',
      'description' => esc_html__('Menu', 'bizness'),
      "params" => array(
            array(
               "type" => "textfield",            
               "class" => "",
               "heading" => esc_html__("Menu Heading", 'bizness'),
               "param_name" => "bizness_menu_heading",
               "value" => '',
               "description" => 'E.g Pages'              
            ),         
            array(
               "type" => "dropdown",            
               "class" => "",
               "heading" => esc_html__("Menu List", 'bizness'),
               "param_name" => "bizness_menu_id",
               "value" => get_all_menu(), 
               "description" => __("", 'bizness'),                       
            ),                                                                      
      )
   )
);

}
?>