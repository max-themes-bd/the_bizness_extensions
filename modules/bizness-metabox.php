<?php
/**
 * Adds a box to the main column on the Post/Page add/edit screens.
 */
function bizness_add_meta_box() {

    add_meta_box(
            'bizness_sectionid', esc_html__( 'Page Header Settings', 'bizness' ), 'bizness_meta_box_callback', 'page', 'normal', 'high'
    ); #you can change the 4th paramter i.e. post to custom post type name, if you want it for something else

    add_meta_box(
            'bizness_sectionid', esc_html__( 'Page Header Settings', 'bizness' ), 'bizness_meta_box_callback', 'bizness-service', 'normal', 'high'
    ); 
                
}

add_action( 'add_meta_boxes', 'bizness_add_meta_box' );

/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current post/page.
 */
function bizness_meta_box_callback( $post ) {

        // Add an nonce field so we can check for it later.
        // wp_nonce_field( 'bizness_meta_box', 'bizness_meta_box_nonce' );
		wp_nonce_field( basename( __FILE__ ), 'bizness_meta_box_nonce' );
            /*
            * Use get_post_meta() to retrieve an existing value
            * from the database and use the value for the form.
            */
            global $post;
            $show_hide_page_header    = get_post_meta( $post->ID, 'show_hide_page_header', true ); 
            $header_solid             = get_post_meta( $post->ID, 'header_solid', true ); 
    		$bizness_page_heading        = get_post_meta( $post->ID, 'bizness_page_heading', true ); 
            $bizness_page_subheading     = get_post_meta( $post->ID, 'bizness_page_subheading', true );
            $bizness_page_bg             = get_post_meta( $post->ID, 'bizness_page_bg', true );
        ?>  
        <div class="wrap">
            <table class="form-table"> 
                <tr valign="top">
                    <th scope="row">
                        <label for="bizness_page_heading"><?php esc_html_e('Show/Hide Page Header', 'bizness')?></label>
                    </th>
                    <td style="vertical-align: middle;">
                        <select name="show_hide_page_header" class="timezone_string">
                            <option value="show" <?php if( $show_hide_page_header == "show" ){ echo "selected=selected"; }?>><?php esc_html_e('Show', 'bizness')?></option>
                            <option value="hide" <?php if( $show_hide_page_header == "hide" ){ echo "selected=selected"; }?>><?php esc_html_e('Hide', 'bizness')?></option>
                        </select>                                
                    </td>
                </tr>
                <!-- End Show/Hide Page Header -->  

                <tr valign="top">
                    <th scope="row">
                        <label for="header_solid"><?php esc_html_e('Header Style', 'bizness')?></label>
                    </th>
                    <td style="vertical-align: middle;">
                        <select name="header_solid" class="timezone_string">
                            <option value="solid_header" <?php if( $header_solid == "solid_header" ){ echo "selected=selected"; }?>><?php esc_html_e('Solid Header', 'bizness')?></option>
                            <option value="transparent_header" <?php if( $header_solid == "transparent_header" ){ echo "selected=selected"; }?>><?php esc_html_e('Transparent Header', 'bizness')?></option>
                        </select>                                
                    </td>
                </tr>
                <!-- End Show/Hide Page Header --> 

                <tr valign="top">
                    <th scope="row">
                        <label for="bizness_page_heading"><?php esc_html_e('Page Heading', 'bizness')?></label>
                    </th>
                    <td style="vertical-align: middle;">
                        <input type="text" name="bizness_page_heading" class="widefat" value="<?php if ($bizness_page_heading) { echo esc_attr($bizness_page_heading); }?>">            
                    </td>
                </tr>
                <!-- End Page Heading -->

                <tr valign="top">
                    <th scope="row">
                        <label for="bizness_page_subheading"><?php esc_html_e('Page Sub Heading', 'bizness')?></label>
                    </th>
                    <td style="vertical-align: middle;">
                        <input type="text" name="bizness_page_subheading" class="widefat" value="<?php if ($bizness_page_subheading) { echo esc_attr($bizness_page_subheading); }?>">            
                    </td>
                </tr>
                <!-- End Page Heading -->

                <tr valign="top">
                    <th scope="row">
                        <label for="bizness_page_bg"><?php esc_html_e('Header Background Image URL', 'bizness')?></label>
                    </th>
                    <td style="vertical-align: middle;">
                        <input type="text" name="bizness_page_bg" class="widefat" value="<?php if ($bizness_page_bg) { echo esc_url($bizness_page_bg); }?>">            
                    </td>
                </tr>
                <!-- End Background Image -->

            </table>
        </div>        
        <?php
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function bizness_save_meta_box_data( $post_id ) {

        
		$is_autosave = wp_is_post_autosave( $post_id );
		$is_revision = wp_is_post_revision( $post_id );
		$is_valid_nonce = ( isset( $_POST[ 'bizness_meta_box_nonce' ] ) && wp_verify_nonce( $_POST[ 'bizness_meta_box_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
    # Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }
 
    # Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'show_hide_page_header' ] ) ) {
        update_post_meta( $post_id, 'show_hide_page_header', sanitize_text_field( $_POST[ 'show_hide_page_header' ] ) );
    } 
    # Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'header_solid' ] ) ) {
        update_post_meta( $post_id, 'header_solid', sanitize_text_field( $_POST[ 'header_solid' ] ) );
    } else {
         update_post_meta( $post_id, 'header_solid', 'solid_header' );
    }    
    # Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'bizness_page_heading' ] ) ) {
        update_post_meta( $post_id, 'bizness_page_heading', sanitize_text_field( $_POST[ 'bizness_page_heading' ] ) );
    }
    if( isset( $_POST[ 'bizness_page_subheading' ] ) ) {
        update_post_meta( $post_id, 'bizness_page_subheading', sanitize_text_field( $_POST[ 'bizness_page_subheading' ] ) );
    }	
    if( isset( $_POST[ 'bizness_page_bg' ] ) ) {
        update_post_meta( $post_id, 'bizness_page_bg', sanitize_text_field( $_POST[ 'bizness_page_bg' ] ) );
    }

}

add_action( 'save_post', 'bizness_save_meta_box_data', 10, 2 );