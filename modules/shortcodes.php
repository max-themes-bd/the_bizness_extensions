<?php
/*-----------------------------------------------------------------------------------*/
/* Creating Shortcodes in order to use in the Visual Composer Page builder plugin */
/*-----------------------------------------------------------------------------------*/

/******************
Video Slider
******************/
if(!function_exists('bizness_video_slider_shortcode')){
function bizness_video_slider_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'bizness_video_url'	=> '',
	'bizness_video_thumb_url'	=> '',
	'bizness_heading_normalt'	=> '',
	'bizness_heading_strongt'	=> '',
	'bizness_heading_normalt2' =>'',
	'bizness_details' => '',
	'bizness_button1_text' => '',
	'bizness_button1_url' => '',
	'bizness_button_text' => '',
	'bizness_button2_text' => '',
	'bizness_button2_url' => ''
	), $atts ) );

	$output = '';

	$output .='
			<!--Slider-->
			<section class="rev_slider_wrapper text-center">			
			<!-- START REVOLUTION SLIDER 5.0 auto mode -->
			  <div id="rev_slider_video" class="rev_slider"  data-version="5.0">
			    <ul>	
			    <!-- SLIDE  -->
			      <li data-transition="fade">
			        <!-- MAIN IMAGE -->
			        <img src="'.esc_url($bizness_video_thumb_url).'" alt="" data-bgposition="center center" data-bgfit="cover" data-bgparallax="10" class="rev-slidebg">							
			        
			        <div class="rs-background-video-layer" 
			         data-volume="mute" 
			         data-videowidth="100%" 
			         data-videoheight="100%" 
			         data-videomp4="'.esc_url($bizness_video_url).'"
			         data-videopreload="preload" 
			         data-videoloop="loop" 
			         data-forceCover="1" 
			         data-aspectratio="16:9" 
			         data-autoplay="true" 
			         data-autoplayonlyfirsttime="true" 
			         data-nextslideatend="true" ></div>
			        
			        <!-- LAYER NR. 1 -->
			        <div class="tp-caption tp-resizeme" 							
			        data-x="[\'center\',\'center\',\'center\',\'center\']" data-hoffset="[\'0\',\'0\',\'0\',\'0\']"
			        data-y="[\'220\',\'150\',\'160\',\'110\']" data-voffset="[\'0\',\'0\',\'0\',\'0\']"						
			        data-responsive_offset="on"
			        data-visibility="[\'on\',\'on\',\'on\',\'on\']"
			        data-transform_idle="o:1;"
			        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" 
			        data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
			        data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"							 
			        data-start="800"><h1>'.$bizness_heading_normalt.' <strong>'.$bizness_heading_strongt.'</strong> '.$bizness_heading_normalt2.'</h1>
			        </div>
			        <div class="tp-caption tp-resizeme" 							
			        data-x="[\'center\',\'center\',\'center\',\'center\']" data-hoffset="[\'0\',\'0\',\'0\',\'0\']"
			        data-y="[\'300\',\'230\',\'230\',\'280\']" data-voffset="[\'0\',\'0\',\'0\',\'0\']"
			        data-responsive_offset="on"
			        data-visibility="[\'on\',\'on\',\'on\',\'off\']"
			        data-transform_idle="o:1;"
			        data-transform_in="opacity:0;s:1000;e:Power2.easeInOut;" 
			        data-transform_out="opacity:0;s:1000;s:1000;" 
			        data-start="1500"><p>'.$bizness_details.'</p>
			        </div>
			        <div class="tp-caption  tp-resizeme" 							
			        data-x="[\'center\',\'center\',\'center\',\'center\']" data-hoffset="[\'0\',\'0\',\'0\',\'0\']"
			        data-y="[\'390\',\'350\',\'310\',\'180\']" data-voffset="[\'0\',\'0\',\'0\',\'0\']"							
			        data-responsive_offset="on"
			        data-visibility="[\'on\',\'on\',\'on\',\'on\']"
			        data-transform_idle="o:1;"
			        data-transform_in="y:[-200%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
			        data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
			        data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
			        data-mask_out="x:0;y:0;s:inherit;e:inherit;" 							 
			        data-start="2000">';
			        if( !empty($bizness_button1_text) ){
				$output .='			        	
			        <a href="'.esc_url($bizness_button1_url).'" class="border_radius btn_common blue">'.$bizness_button1_text.'</a>';
			        }
			        if( !empty($bizness_button2_text) ){			        
			    $output .='	
			        <a href="'.esc_url($bizness_button2_url).'" class="btn_common white_border">'.$bizness_button2_text.'</a>';
			    }
				$output .='			    
			        </div>
			      </li>
			    </ul>				
			  </div><!-- END REVOLUTION SLIDER -->
			</section>
			';

	return $output; 
}

add_shortcode('bizness_video_slider', 'bizness_video_slider_shortcode');
}
/******************
Image Slider
******************/
if(!function_exists('bizness_image_slider_shortcode')){
function bizness_image_slider_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'number_of_slider'	=> ''
	), $atts ) );

	$output = '';
	$output .= '
			<!--Slider-->
			<section class="rev_slider_wrapper text-center">			
			<!-- START REVOLUTION SLIDER 5.0 auto mode -->
			  <div id="rev_slider_full" class="rev_slider"  data-version="5.0">
			    <ul>	
			    <!-- SLIDE  -->';

				$i=1;
				$c=0;
				while ($i <= $number_of_slider){
				$c++;
				$b = shortcode_atts(array(
					'bizness_img_path_'.$c.'' => '',
					'bizness_heading_normalt_'.$c.'' => '',
					'bizness_heading_strongt_'.$c.'' => '',
					'bizness_heading_normalt2_'.$c.'' => '',
					'bizness_details_'.$c.'' => '',
					'bizness_button1_text_'.$c.'' => '',
					'bizness_button1_url_'.$c.'' => '',
					'bizness_button2_text_'.$c.'' => '',
					'bizness_button2_url_'.$c.'' => '',
				),$atts);
					
				$bizness_img_path 				=$b['bizness_img_path_'.$c.''];
				$bizness_heading_normalt 		=$b['bizness_heading_normalt_'.$c.''];
				$bizness_heading_strongt 		=$b['bizness_heading_strongt_'.$c.''];
				$bizness_heading_normalt2 		=$b['bizness_heading_normalt2_'.$c.''];
				$bizness_details 				=$b['bizness_details_'.$c.''];	
				$bizness_button1_text 			=$b['bizness_button1_text_'.$c.''];
				$bizness_button1_url 			=$b['bizness_button1_url_'.$c.''];
				$bizness_button2_text 			=$b['bizness_button2_text_'.$c.''];	
				$bizness_button2_url 			=$b['bizness_button2_url_'.$c.''];

				$featured_img = wp_get_attachment_image_src($bizness_img_path, 'full');			    
	$output .= '			    
			      <li data-transition="fade">
			        <!-- MAIN IMAGE -->
			        <img src="'.esc_url($featured_img[0]).'" alt="" data-bgposition="center center" data-bgfit="cover" data-bgparallax="10" class="rev-slidebg">							
			        <!-- LAYER NR. 1 -->
			        <div class="tp-caption tp-resizeme" 							
			        data-x="[\'center\',\'center\',\'center\',\'center\']" data-hoffset="[\'0\',\'15\',\'15\',\'15\']"
			        data-y="[\'306\',\'250\',\'270\',\'150\']" data-voffset="[\'0\',\'0\',\'0\',\'0\']"						
			        data-responsive_offset="on"
			        data-visibility="[\'on\',\'on\',\'on\',\'on\']"
			        data-transform_idle="o:1;"
			        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" 
			        data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
			        data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"							 
			        data-start="800"><h1>'.$bizness_heading_normalt.' <strong>'.$bizness_heading_strongt.'</strong> '.$bizness_heading_normalt2.'</h1>
			        </div>
			        <div class="tp-caption tp-resizeme" 							
			        data-x="[\'center\',\'center\',\'center\',\'center\']" data-hoffset="[\'0\',\'15\',\'15\',\'15\']"
			        data-y="[\'380\',\'340\',\'300\',\'350\']" data-voffset="[\'0\',\'0\',\'0\',\'0\']"
			        data-responsive_offset="on"
			        data-visibility="[\'on\',\'on\',\'off\',\'off\']"
			        data-transform_idle="o:1;"
			        data-transform_in="opacity:0;s:1000;e:Power2.easeInOut;" 
			        data-transform_out="opacity:0;s:1000;s:1000;" 
			        data-start="1500"><p>'.$bizness_details.'</p>
			        </div>
			        <div class="tp-caption  tp-resizeme" 							
			        data-x="[\'center\',\'center\',\'center\',\'center\']" data-hoffset="[\'0\',\'15\',\'15\',\'15\']"
			        data-y="[\'470\',\'410\',\'350\',\'250\']" data-voffset="[\'0\',\'0\',\'0\',\'0\']"							
			        data-responsive_offset="on"
			        data-visibility="[\'on\',\'on\',\'on\',\'on\']"
			        data-transform_idle="o:1;"
			        data-transform_in="y:[-200%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
			        data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
			        data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
			        data-mask_out="x:0;y:0;s:inherit;e:inherit;" 							 
			        data-start="2000">';
			        if( !empty($bizness_button1_text) ){
				$output .= '			        	
			        <a href="'.esc_url($bizness_button1_url).'" class="border_radius btn_common blue">'.$bizness_button1_text.'</a>';
			    	}
			        if( !empty($bizness_button2_text) ){
				$output .= '			        			    	
			        <a href="'.esc_url($bizness_button2_url).'" class="border_radius btn_common yellow">'.$bizness_button2_text.'</a>';
			    	}
				$output .= '			    	
			        </div>
			      </li>';
			    $i++;
			    }  

	$output .= '			      
			    </ul>
			  </div><!-- END REVOLUTION SLIDER -->
			</section>	
	';

	return $output; 
}

add_shortcode('bizness_image_slider', 'bizness_image_slider_shortcode');
}
/******************
Heading
******************/
if(!function_exists('bizness_common_heading_shortcode')){
function bizness_common_heading_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'bizness_heading_normalt'	=> '',
	'bizness_heading_strongt' =>'',
	'bizness_subheading' => ''
	), $atts ) );

	$output = '';

	$output .='<div class="text-center wow fadeInDown">';
	$output .='
        <h2 class="heading"><span>'.$bizness_heading_normalt.'</span> '.$bizness_heading_strongt.' <span class="divider-center"></span></h2>';
    if( !empty($bizness_subheading) ){   
    $output .='    
        <p class="heading_space margin10">'.$bizness_subheading.'</p>';
    }    
	$output .='</div>';

	return $output; 
}

add_shortcode('bizness_common_heading', 'bizness_common_heading_shortcode');
}
/******************
Left Align Heading
******************/
if(!function_exists('bizness_left_align_heading_shortcode')){
function bizness_left_align_heading_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'bizness_heading_normalt'	=> '',
	'bizness_heading_strongt' =>''
	), $atts ) );

	$output = '';

	$output .='<div class="wow fadeInDown">';
	$output .='
        <h2 class="heading heading_space"><span>'.$bizness_heading_normalt.'</span> '.$bizness_heading_strongt.' <span class="divider-left"></span></h2>';   
	$output .='</div>';

	return $output; 
}

add_shortcode('bizness_left_align_heading', 'bizness_left_align_heading_shortcode');
}
/******************
Services
******************/
if(!function_exists('bizness_services_shortcode')){
function bizness_services_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'bizness_heading'	=> '',
	'bizness_icon' =>'',
	'bizness_details' => '',
	'bizness_hcolor' => ''
	), $atts ) );

	$output = '';

	$get_rand = mt_rand();

	if( !empty($bizness_hcolor) ){
	$output .= '
		<style>
			.iconwrap'.$get_rand.' .icon_box:hover{
				background: '.$bizness_hcolor.' !important;
			}
			.iconwrap'.$get_rand.' .icon_box i{
				color: '.$bizness_hcolor.'!important;
			}
			.iconwrap'.$get_rand.' .icon_box:hover i{
				color:#fff !important;
			}
		</style>	
	';	
	}

	$output .='<div class="icon_wrapclearfix iconwrap'.$get_rand.'">';
	$output .='<div class="icon_box text-center margin_tophalf wow fadeInUp" data-wow-delay="300ms">
		        <i class="'.$bizness_icon.'"></i>
		        <h4 class="text-capitalize bottom20 margin10">'.$bizness_heading.'</h4>
		        <p class="no_bottom">'.$bizness_details.'</p>
		      </div>
		    '; 
	$output .='</div>';

	return $output; 
}

add_shortcode('bizness_services', 'bizness_services_shortcode');
}
/******************
Call To Action
******************/
if(!function_exists('bizness_call_to_action_shortcode')){
function bizness_call_to_action_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'bizness_heading'	=> '',
	'bizness_subheading' =>'',
	'bizness_bgimg_url' => '',
	'bizness_btn_text' => '',
	'bizness_btn_url' => ''
	), $atts ) );
	$output = $bgimg = '';
	$featured_img = wp_get_attachment_image_src($bizness_bgimg_url, 'full');	

	if( !empty($featured_img[0]) ){
		$bgimg = 'style="background:url('.$featured_img[0].') no-repeat;background-size: cover;width: 100%;background-attachment: fixed;"';
	}

	$output .= '
			<!--Paralax -->
			<section id="parallax" class="parallax" '.$bgimg.'>
			  <div class="container">
			    <div class="row">
			      <div class="col-md-12 text-center wow bounceIn">
			       <p class="bottom25 whitecolor">'.$bizness_subheading.'</p>
			       <h1 class="bottom25 whitecolor">'.$bizness_heading.'</h1>
			       <a href="'.esc_url($bizness_btn_url).'" class="border_radius btn_common white_border">'.$bizness_btn_text.'</a>
			      </div>
			    </div>
			  </div>
			</section>
			<!--Paralax -->
		    '; 

	return $output; 
}

add_shortcode('bizness_call_to_action', 'bizness_call_to_action_shortcode');
}

/******************
Gallery/Portfolio
******************/
if(!function_exists('bizness_gallery_portfolio_shortcode')){
function bizness_gallery_portfolio_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'bizness_heading_normalt'	=> '',
	'bizness_heading_strongt' =>''
	), $atts ) );
	$output = '';

	$output .= '
			<!-- Gallery -->
			<section id="gallery" class="padding">
			  <div class="container">
			    <div class="row">';
	$output .= '		    
			      <div class="col-sm-4">
			        <h2 class="heading heading_space"><span>'.$bizness_heading_normalt.'</span> '.$bizness_heading_strongt.'<span class="divider-left"></span></h2>
			      </div>
			      <div class="col-sm-8">
			        <div id="project-filter" class="cbp-l-filters">
			          <div data-filter="*" class="cbp-filter-item-active cbp-filter-item">ALL IMAGES</div>';
						$taxonomies = array('taxonomy' => 'gallery_cat');
						$taxonomy_terms = get_terms( $taxonomies );
						foreach ( $taxonomy_terms as $taxonomy_term ) :
			$output .= '<div data-filter=".'.$taxonomy_term->term_id.'" class="cbp-filter-item">'.$taxonomy_term->name.' </div>';
						endforeach;
	$output .= '
			      </div>
			    </div>
				</div>
			    ';

	$output .='<div id="projects" class="cbp">';

			$args = array(
				'post_type' 		=> 'bizness-gallery',
				'order'				=> 'desc',
				'posts_per_page' 	=> -1	
			);
			$query  = new WP_Query( $args );
			if ($query->have_posts()) :  while ($query->have_posts()) : $query->the_post();
			$bizness_global_post 				= bizness_get_global_post();
			$postid 						= $bizness_global_post->ID;
			$get_image 						= esc_url( wp_get_attachment_url( get_post_thumbnail_id($postid) ) );

			$terms = get_the_terms(get_the_ID(), 'gallery_cat' );
			if ($terms && ! is_wp_error($terms)) :
			    foreach ($terms as $term) {
			        $get_cate_id = $term->term_id;
			    }
			endif;	
		$output .='<div class="cbp-item '.$get_cate_id.'">';	

		      if( !empty(esc_url( $get_image )) ):
		    $output .= '
		        <img src="'.$get_image.'" alt="">';
			endif;

		$output .='
		        <div class="overlay">
		          <div class="centered text-center">
		            <a href="'.$get_image.'" class="cbp-lightbox opens"> <i class=" icon-expand"></i></a> 
		          </div>
		        </div>
			';

		$output .='</div>';		
			endwhile; endif;

	$output .='</div>';	

	$output .= '			    
			    </div>
			  </div>
			</section>
			<!-- Gallery -->
		    '; 

	return $output; 
}

add_shortcode('bizness_gallery_portfolio', 'bizness_gallery_portfolio_shortcode');
}
/******************
Stats Section
******************/
if(!function_exists('bizness_shortcodes_stats_shortcode')){
function bizness_shortcodes_stats_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'stat_bgimg_url'	=> '',
	'stat_title1'	=> '',
	'stat_icon1' =>'',
	'stat_number1' => '',
	'stat_title2' => '',
	'stat_icon2' => '',
	'stat_number2' => '',
	'stat_title3' => '',
	'stat_icon3' => '',
	'stat_number3' => '',
	'stat_title4' => '',
	'stat_icon4' => '',
	'stat_number4' => ''
	), $atts ) );
	$output = $bgimg = '';

	$featured_img = wp_get_attachment_image_src($stat_bgimg_url, 'full');

	if( !empty($featured_img[0]) ){
		$bgimg = 'style="background:url('.$featured_img[0].') no-repeat;background-size: cover;background-attachment: fixed;width: 100%;"';
	}

	$output .= '
			<!--Fun Facts-->
			<section id="counter" class="parallax padding" '.$bgimg.'>
			  <div class="container">
			    <h2 class="hidden">hidden</h2>
			    <div class="row number-counters">
			      <div class="col-md-3 col-sm-6 col-xs-6 counters-item text-center wow fadeInUp" data-wow-delay="300ms">
			        <i class="'.$stat_icon1.'"></i>
			        <strong data-to="'.esc_attr($stat_number1).'">0</strong>
			        <p>'.$stat_title1.'</p>
			      </div>
			      <div class="col-md-3 col-sm-6 col-xs-6 counters-item text-center wow fadeInUp" data-wow-delay="400ms">
			        <i class="'.$stat_icon2.'"></i>
			        <strong data-to="'.esc_attr($stat_number2).'">0</strong>
			        <p>'.$stat_title2.'</p>
			      </div>
			      <div class="col-md-3 col-sm-6 col-xs-6 counters-item text-center wow fadeInUp" data-wow-delay="500ms">
			        <i class=" '.$stat_icon3.'"></i>
			        <strong data-to="'.esc_attr($stat_number3).'">0</strong>
			        <p>'.$stat_title3.'</p>
			      </div>
			      <div class="col-md-3 col-sm-6 col-xs-6 counters-item text-center wow fadeInUp" data-wow-delay="600ms">
			        <i class="'.$stat_icon4.'"></i>
			        <strong data-to="'.esc_attr($stat_number4).'">0</strong>
			        <p>'.$stat_title4.'</p>
			      </div>
			    </div>
			  </div>
			</section>
			<!--Fun Facts-->
		    '; 

	return $output; 
}

add_shortcode('bizness_shortcodes_stats', 'bizness_shortcodes_stats_shortcode');
}
/******************
Testimonials
******************/
if(!function_exists('bizness_testimonials_shortcode')){
function bizness_testimonials_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'bizness_heading_normalt'	=> '',
	'bizness_heading_strongt' =>'',
	'bizness_display_limits' => '',
	'bizness_display_order' => ''
	), $atts ) );
	$output = '';

	$output .= '
			<!--Customers Review-->
			<section id="reviews" class="padding bg_light">
			  <div class="container">
			    <div class="row">
			      <div class="col-md-12 text-center wow fadeInDown">
			      <h2 class="heading heading_space"><span>'.$bizness_heading_normalt.'</span> '.$bizness_heading_strongt.' <span class="divider-center"></span></h2>
			      <div id="review_slider" class="owl-carousel text-center">';
					
					$args = array(
						'post_type' 		=> 'bizness-testimonial',
						'order'				=> $bizness_display_order,
						'posts_per_page' 	=> $bizness_display_limits	
					);
					$query  = new WP_Query( $args );
					if ($query->have_posts()) :  while ($query->have_posts()) : $query->the_post();
					$bizness_global_post 				= bizness_get_global_post();
					$postid 						= $bizness_global_post->ID;
					$get_image 						= esc_url( wp_get_attachment_url( get_post_thumbnail_id($postid) ) );
					$testimonial_designation  		= get_post_meta($postid, 'testimonial_designation', true);
			$output .= '
			        <div class="item">
			          <img src="'.esc_url($get_image).'" class="client_pic border_radius" alt="">
			          <p class="review_text">'.get_the_content().'</p>
			          <h4>'.get_the_title().'</h4>
			          <p>'.esc_attr($testimonial_designation).'</p>
			        </div>';

			        endwhile; endif;
	$output .= '
			       </div>
			      </div>
			    </div>
			  </div>
			</section>
			<!--Customers Review--> 
		    '; 

	return $output; 
}

add_shortcode('bizness_testimonials', 'bizness_testimonials_shortcode');
}
/******************
Testimonials
******************/
if(!function_exists('bizness_pricing_tables_shortcode')){
function bizness_pricing_tables_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'bizness_heading'	=> '',
	'bizness_currency_symbol' =>'',
	'bizness_price' => '',
	'bizness_description' => '',
	'bizness_bullet_text' => '',
	'bizness_button_text' => '',
	'bizness_button_url' => '',
	'bizness_isactive' => ''

	), $atts ) );
	$output = '';

	if(empty($bizness_currency_symbol)){
		$bizness_currency_symbol = "$";
	} else {
		$bizness_currency_symbol = $bizness_currency_symbol;
	}

	if( $bizness_isactive == true ){
		$class_active = "active";
		$class_bar = "blue";
	} else {
		$class_active = "";
		$class_bar = "btn_border";		
	}

	$output .= '
			<!--Pricings-->
		        <div class="pricing">';
	$output .= '		        
		          <div class="pricing_item '.$class_active.' wow fadeInUp" data-wow-delay="300ms">
		            <h3>'.$bizness_heading.'</h3>
		            <div class="pricing_price"><span class="pricing_currency">'.$bizness_currency_symbol.'</span>'.$bizness_price.'</div>
		            <p class="pricing_sentence">'.$bizness_description.'</p>';
				if(!empty($bizness_bullet_text)){
					$bizness_bullet_text = !empty($bizness_bullet_text) ? explode("\n", trim($bizness_bullet_text)) : array(); 
					$output.= '	
					<ul class="pricing_list">';
						foreach($bizness_bullet_text as $xbulltet) {
						$output.='<li class="pricing_feature">'.$xbulltet.'</li>';
						}
						$output.= '
					</ul>';
				}		            
	$output .= '	            
		            <a class="btn_common '.$class_bar.' text-center" href="'.esc_url($bizness_button_url).'">'.$bizness_button_text.'</a>
		          </div>';
	$output .= '		          
		        </div>
			<!--Pricings-->
		    '; 

	return $output; 
}

add_shortcode('bizness_pricing_tables', 'bizness_pricing_tables_shortcode');
}

/******************
Team
******************/
if(!function_exists('bizness_team_shortcode')){
function bizness_team_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'bizness_bgimg_url'	=> '',
	'bizness_heading_normalt' =>'',
	'bizness_heading_strongt' => '',
	'bizness_display_order' => '',
	'bizness_nametext_color' => '',
	'bizness_detailstext_color' => ''

	), $atts ) );

	$output = $name_style = $details_style = $bgimg = '';
	$featured_img = wp_get_attachment_image_src($bizness_bgimg_url, 'full');

	if( !empty($bizness_bgimg_url) ){
		$bgimg = 'style="background:url('.$featured_img[0].') no-repeat;background-size: cover;width: 100%;background-attachment: fixed;background-position: center center;"';
	}

	if( $bizness_nametext_color !='' ){
		$name_style='style="color:'.$bizness_nametext_color.';"';
	}  	

	if( $bizness_detailstext_color !='' ){
		$details_style='style="color:'.$bizness_detailstext_color.';"';
	}  



	$output .= '
			<!-- experts -->
			<section id="experts" class="padding parallax" '.$bgimg.'>
			  <div class="container">
			    <div class="row">
			      <div class="col-md-12 wow fadeInDown">
			        <h2 class="heading heading_space" '.$name_style.'><span>'.$bizness_heading_normalt.' </span> '.$bizness_heading_strongt.'<span class="divider-left"></span></h2>
			      </div>
			    </div>
			    <div class="row">
			      <div class="col-md-12">
			        <div class="slider_wrapper">
			          <div id="expert_slider" class="owl-carousel">';
      				$args = array( 'post_type'=> 'bizness-team', 'order'=> $bizness_display_order );
      				$query = new WP_Query($args);
      				if ($query->have_posts()) :  while ($query->have_posts()) : $query->the_post();
					$bizness_global_post 				= bizness_get_global_post();
					$postid 						= $bizness_global_post->ID;
					$get_image 						= esc_url( wp_get_attachment_url( get_post_thumbnail_id($postid) ) );
					$team_designation       		= get_post_meta($postid, 'team_designation', true);
					$team_fb                		= get_post_meta($postid, 'team_fb', true);
					$team_twitter           		= get_post_meta($postid, 'team_twitter', true);
					$team_dribbble          		= get_post_meta($postid, 'team_dribbble', true);  					
					$team_instagram          		= get_post_meta($postid, 'team_instagram', true);  					

				$output .= '			          
			            <div class="item">
			              <div class="image bottom20">
			                <img src="'.$get_image.'" alt="experts" class="img-responsive border_radius">
			                <div class="overlay">
			                  <div class="centered">
			                  <ul class="social_icon">';
			            if( !empty($team_fb) ){
			            $output .= '     
			                  <li><a href="'.esc_url($team_fb).'" class="facebook"><i class="fa fa-facebook"></i></a></li>';
			            }
			            if( !empty($team_twitter) ){      
			            $output .= '      
			                  <li><a href="'.esc_url($team_twitter).'" class="twitter"><i class="icon-twitter4"></i></a></li>';
			            }
			            if( !empty($team_dribbble) ){      
			            $output .= '      
			                  <li><a href="'.esc_url($team_dribbble).'" class="dribble"><i class="icon-dribbble5"></i></a></li>';
			            }
			            if( !empty($team_instagram) ){      
			            $output .= '      
			                  <li><a href="'.esc_url($team_instagram).'" class="instagram"><i class="icon-instagram"></i></a></li>';
			            }      

			    $output .= '
			                </ul>
			                  </div>
			                </div>
			              </div>
			              <h3 class="bottom5" '.$name_style.'>'.get_the_title().'</h3>
			              <h5 class="color bottom15"><strong>'.esc_attr($team_designation).'</strong></h5>
			              <p class="bottom15" '.$details_style.'>'.get_the_excerpt().'</p>
			            </div>';
			        endwhile; endif;    
				$output .= '				            
			          </div>
			        </div>
			      </div>
			    </div>
			  </div>
			</section>
			<!-- experts -->
		    '; 

	return $output; 
}

add_shortcode('bizness_team', 'bizness_team_shortcode');
}
/******************
Recent News
******************/
if(!function_exists('bizness_custom_excerpt')){
function bizness_custom_excerpt($excerpt_lenght){
    $excerpt = get_the_content();
    $excerpt = preg_replace(" ([.*?])",'',$excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $excerpt_lenght);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace( '/s+/', ' ', $excerpt));
    return $excerpt;
}
}

if(!function_exists('bizness_recent_news_shortcode')){
function bizness_recent_news_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'bizness_display_layout' 		=>'',
	'bizness_heading_normalt' 		=>'',
	'bizness_heading_strongt' 		=> '',
	'bizness_post_category'			=> '',
	'bizness_post_limits'			=> '',
	'bizness_display_order' 		=> ''
	), $atts ) );

	$output ='';
	if( empty($bizness_display_layout) || (int)$bizness_display_layout == 1 ){

	$output .= '
			<!-- News-->
			<section id="news" class="padding">
			  <div class="container">
			    <div class="row">
			      <div class="col-md-12 wow fadeInDown">
			       <h2 class="heading heading_space"><span>'.$bizness_heading_normalt.'</span> '.$bizness_heading_strongt.' <span class="divider-left"></span></h2>
			      </div>
			    </div>
			    <div class="row">';
					$args = array(
				        'post_type' 		=> 'post', 
				        'cat'				=> $bizness_post_category,             
				        'order' 			=> $bizness_display_order,
				        'post_status' 		=> 'publish',
				        'posts_per_page' 	=> $bizness_post_limits
				    ); 
				    $query = new WP_Query( $args );
					if ($query->have_posts()) :  while ($query->have_posts()) : $query->the_post();
					$bizness_global_post = bizness_get_global_post();
					$postid = $bizness_global_post->ID;
					$get_image = esc_url( wp_get_attachment_url( get_post_thumbnail_id($postid) ) );	
				$output .= '					
				      <div class="col-md-4 col-sm-6 col-xs-12">
				        <div class="blog-wrap">
				          <div class="blog-img">
				            <img src="'.$get_image.'" />
				          </div>
				          <div class="blog-content">
				            <div class="blog-meta">
				              <ul>
				                <li><a href="'.get_the_author_link().'"><i class="fa fa-user"></i> '.esc_html__('By', 'bizness').' '.get_the_author().' </a></li>';
				                $comment_count = get_comments_number( $postid );
				                if( $comment_count == 0 || $comment_count > 1 ){
							$output .= '
				                <li><a href="'.get_the_permalink().'"><i class="fa fa-comment"></i> '.$comment_count.' Comments </a></li>';                	
				                } else {
				            $output .= '
				                <li><a href="'.get_the_permalink().'"><i class="fa fa-comment"></i> '.$comment_count.' Comment </a></li>';
				                }
				            $output .= '    
				              </ul>
				            </div>
				            <h3><a href="'.get_the_permalink().'">'.get_the_title().'</a></h3>
				            <p>'.bizness_custom_excerpt(56).'</p>
				            <a href="'.get_the_permalink().'" class="readmore">'.esc_html__('Read More', 'bizness').'</a>
				          </div>
				        </div>
				      </div>';
		            endwhile; endif;
		            wp_reset_postdata();	
			$output .= '
			    </div>
			  </div>
			</section>
		    '; 		

	} else {

	$output .= '
			<!-- News-->
			<section id="news" class="padding">
			  <div class="container">
			    <div class="row">
			      <div class="col-md-12 wow fadeInDown">
			       <h2 class="heading heading_space"><span>'.$bizness_heading_normalt.'</span> '.$bizness_heading_strongt.' <span class="divider-left"></span></h2>
			      </div>
			    </div>
			    <div class="row">
			      <div class="col-md-12">
			        <div class="slider_wrapper">
			          <div id="news_slider" class="owl-carousel">';

						$args = array(
					        'post_type' => 'post', 
					        'cat'=>$bizness_post_category,             
					        'order' => $bizness_display_order,
					        'post_status' => 'publish',
					        'posts_per_page' => $bizness_post_limits
					    ); 
					    $query = new WP_Query( $args );
						if ($query->have_posts()) :  while ($query->have_posts()) : $query->the_post();
						$bizness_global_post = bizness_get_global_post();
						$postid = $bizness_global_post->ID;
						$get_image = esc_url( wp_get_attachment_url( get_post_thumbnail_id($postid) ) );			          
			$output .= '			          
			            <div class="item">
			              <div class="content_wrap">
			              	<div class="recent_post_thumb">
			                	<div class="image img-responsive border_radius" style="background:url('.$get_image.') no-repeat center center;background-size:cover;"></div>
			                </div>
			                <div class="news_box border_radius">
			                  <h4><a href="'.get_the_permalink().'">'.get_the_title().'</a></h4>
			                  <ul class="commment">
			                    <li><a href="'.get_the_permalink().'"><i class="icon-icons20"></i>'.get_the_time( get_option('date_format') ).'</a></li>
			                    <li><a href="'.get_the_permalink().'"><i class="icon-comment"></i> '.get_comments_number( $postid ).'</a></li>
			                  </ul>
			                  <p>'.bizness_custom_excerpt(56).'...</p>
			                  <a href="'.get_the_permalink().'" class="readmore">'.esc_html_e('Read More', 'bizness').'</a>
			                </div>
			              </div>
			            </div>';
						endwhile; endif;			            
			$output .= '
			          </div>
			        </div>
			      </div>
			    </div>
			  </div>
			</section>
		    '; 

	}

	return $output; 
}

add_shortcode('bizness_recent_news', 'bizness_recent_news_shortcode');
}
/******************
Contact Address
******************/
if(!function_exists('bizness_contact_address_shortcode')){
function bizness_contact_address_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'bizness_heading_normalt' =>'',
	'bizness_heading_strongt' => '',
	'bizness_subheading'	=> '',
	'bizness_location'	=> '',
	'bizness_email' => '',
	'bizness_contactno' => ''
	), $atts ) );

	$output ='';

	$output .= '
		      <div class="contact_address heading_space wow fadeInLeft" data-wow-delay="400ms">
		        <h2 class="heading heading_space"><span>'.$bizness_heading_normalt.'</span> '.$bizness_heading_strongt.' <span class="divider-left"></span></h2>
		        <p>'.$bizness_subheading.'</p>
		        <div class="address">
		          <i class="icon icon-map-pin border_radius"></i>
		          <h4>Visit Us</h4>
		          <p>'.$bizness_location.'</p>
		        </div>
		        <div class="address second">
		          <i class="icon icon-envelope border_radius"></i>
		          <h4>Email Us</h4>
		          <p><a href="mailto:'.$bizness_email.'">'.$bizness_email.'</a></p>
		        </div>
		        <div class="address">
		          <i class="icon icon-phone4 border_radius"></i>
		          <h4>Call Us</h4>
		          <p>'.$bizness_contactno.'</p>
		        </div>
		      </div>
		    '; 

	return $output; 
}
add_shortcode('bizness_contact_address', 'bizness_contact_address_shortcode');
}
/******************
Map
******************/
if(!function_exists('bizness_map_shortcode')){
function bizness_map_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'bizness_lat_long' =>''
	), $atts ) );

	$output ='';

	$output .= '
		    <div class="row wow bounceIn" data-wow-delay="300ms">
		      <div class="col-md-12">
		        <div id="map"></div>
		      </div>
		    </div>
		    '; 

	return $output; 
}
add_shortcode('bizness_map', 'bizness_map_shortcode');
}

/******************
Welcome Note
******************/
if(!function_exists('bizness_welcome_note_shortcode')){
function bizness_welcome_note_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'bizness_heading_normalt' =>'',
	'bizness_heading_strongt' => '',
	'bizness_subheading' => '',
	'bizness_deatils' =>''
	), $atts ) );

	$output ='';

	$output .= '
	      <div class="col-md-12 wow fadeInLeft" data-wow-delay="300ms">
	       	<h2 class="heading heading_space"><span>'.$bizness_heading_normalt.' </span>'.$bizness_heading_strongt.' <span class="divider-left"></span></h2>
	       	<h4 class="bottom25">'.$bizness_subheading.'</h4>
	       	'.$bizness_deatils.'
	      </div>
		    '; 

	return $output; 
}
add_shortcode('bizness_welcome_note', 'bizness_welcome_note_shortcode');
}
/******************
Featured Image
******************/
if(!function_exists('bizness_featured_image_shortcode')){
function bizness_featured_image_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'bizness_img' =>''
	), $atts ) );

	$featured_img = wp_get_attachment_image_src($bizness_img, 'full');

	$output ='';

	$output .= '
	        <div class="image">
	         	<img src="'.esc_url($featured_img[0]).'" alt="">
	        </div>
		    '; 

	return $output; 
}
add_shortcode('bizness_featured_image', 'bizness_featured_image_shortcode');
}

/******************
Content Box
******************/
if(!function_exists('bizness_content_box_shortcode')){
function bizness_content_box_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'bizness_green_text' =>'',
	'bizness_heading' =>'',
	'bizness_details' =>'',
	'bizness_img' =>''
	), $atts ) );

	$featured_img = wp_get_attachment_image_src($bizness_img, 'full');

	$output ='';
	$output .= '
			<section id="history">
			  <div class="row">
			    <div class="col-md-12 col-sm-12 history_wrap bottom25 wow fadeIn" data-wow-delay="300ms">
			      <div class="row">
			        <div class="col-lg-5 col-md-5 col-sm-12">
			          <div class="image"><img src="'.$featured_img[0].'" alt=""></div>
			        </div>
			        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
			          <h3><span>'.$bizness_green_text.'</span> '.$bizness_heading.'</h3>
			          <p>'.$bizness_details.'</p>
			        </div>
			      </div>
			    </div>
			  </div>
			</section>
		    '; 

	return $output; 
}
add_shortcode('bizness_content_box', 'bizness_content_box_shortcode');
}
/******************
About Company
******************/
if(!function_exists('bizness_about_company_shortcode')){
function bizness_about_company_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'bizness_heading_normalt' =>'',
	'bizness_heading_strongt' =>'',
	'bizness_subheading' =>'',
	'bizness_details' =>'',
	'bizness_button_text' =>'',
	'bizness_button_url' =>'',
	'bizness_img' =>''	
	), $atts ) );
	$featured_img = wp_get_attachment_image_src($bizness_img, 'full');

	$output ='';
	$output .= '
			  <div class="tour_media">
			      <div class="tour_body wow fadeInLeft" data-wow-delay="300ms">
			        <h2 class="heading heading_space"><span>'.$bizness_heading_normalt.'  </span> '.$bizness_heading_strongt.'<span class="divider-left"></span></h2>
			        <h4 class="bottom25">'.$bizness_subheading.'</h4>
			        <p class="bottom25">'.$bizness_details.'</p>
			        <a class="btn_common blue border_radius" href="'.esc_url($bizness_button_url).'">'.$bizness_button_text.'</a>
			      </div>
			      <div class="tour_feature wow fadeInRight" data-wow-delay="300ms">
			        <img src="'.esc_url($featured_img[0]).'" alt="">
			      </div>			      
			  </div>
		    '; 

	return $output; 
}
add_shortcode('bizness_about_company', 'bizness_about_company_shortcode');
}
/******************
Thumbnail Image Gallery
******************/
if(!function_exists('bizness_thumbnail_image_row_shortcode')){
function bizness_thumbnail_image_row_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'bizness_gallery_cat' =>'',
	'bizness_image_limits' =>''
	), $atts ) );

	$output ='';
	$output .= '
			  <div class="container">
			      <div id="projects" class="cbp cbp2 cbp-thumb-gallery">';
					$args = array(
			            'post_type' => 'bizness-gallery',              
			            'order' => 'DESC',
			            'post_status' => 'publish',
			            'posts_per_page' => $bizness_image_limits,
			            'offset'=> 1,
				        'tax_query' => array(
				            array(
				                'taxonomy' => 'gallery_cat',
				                'field' => 'term_id',
				                'terms'    => $bizness_gallery_cat
				            )
				        )
			        ); 

				    $query = new WP_Query( $args );
					if ($query->have_posts()) :  while ($query->have_posts()) : $query->the_post();
					$bizness_global_post = bizness_get_global_post();
					$postid = $bizness_global_post->ID;
					$get_image = esc_url( wp_get_attachment_url( get_post_thumbnail_id($postid) ) );
				$output .= '									      
			        <div class="cbp-item">
			          	<img src="'.esc_url($get_image).'" alt="">
			          	<div class="overlay">
				          <div class="centered text-center">
				            <a href="'.esc_url($get_image).'" class="cbp-lightbox opens"> <i class=" icon-expand"></i></a> 
				          </div>
			        	</div>   
			        </div>';
			        endwhile; endif;
		$output .= '			        
			      </div>
			  </div>
		    '; 

	return $output; 
}
add_shortcode('bizness_thumbnail_image_row', 'bizness_thumbnail_image_row_shortcode');
}
/******************
Menu
******************/
if(!function_exists('bizness_menu_shortcode')){
function bizness_menu_shortcode( $atts ) {
	extract( shortcode_atts( array(
	'bizness_menu_heading' =>'',
	'bizness_menu_id' =>''
	), $atts ) );
	$menu = wp_get_nav_menu_object($bizness_menu_id);
	$menu_items = wp_get_nav_menu_items($menu->term_id);

	$output = '';
	$output .= '<div class="col-menu">';
	if( $bizness_menu_heading !='' ){
		$output .= '<h6 class="title">'.$bizness_menu_heading.'</h6>';
	}
		$output .= '<div class="content">';
			$output .= '<ul class="menu-col">';
			foreach( $menu_items as $menu_item ):	
				$output .='<li><a href="'.esc_url($menu_item->url).'">'.$menu_item->title.'</a></li>';
			endforeach;	
			$output .= '</ul>';
		$output .= '</div>';
	$output .= '</div>';

	return $output;

}	
add_shortcode('bizness_menu', 'bizness_menu_shortcode');
}

if(class_exists('WPBakeryVisualComposerAbstract')) {
	include_once('vc_shortcodes.php');
}