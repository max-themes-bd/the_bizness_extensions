<?php
/*
Plugin Name: The Bizness Extensions
Plugin URI: http://themeforest.net
Description: Extensions for Bizness Business Wordpress Theme. Supplied as a separate plugin so that the users do not find empty shortcodes on changing the theme.
Version: 1.0.0
Author: Max-Themes
Author URI: http://themeforest.net/user/max-themes-bd
*/

//Load Modules

#-----------------------------------------------------------------
# Theme Shortcodes
#-----------------------------------------------------------------

require_once 'modules/shortcodes.php';

#-----------------------------------------------------------------
# Mega Menu Post Type
#-----------------------------------------------------------------

require_once 'modules/bizness-post-type.php';

#-----------------------------------------------------------------
# Events MetaBox
#-----------------------------------------------------------------
require_once 'modules/bizness-metabox.php';